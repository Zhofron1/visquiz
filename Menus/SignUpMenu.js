import React, { useState } from 'react';
import {View, Text, Image, ScrollView, TextInput, SafeAreaView, Button, Alert} from 'react-native';
import {StyleSheet} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FormButton from '../components/FormButton';
import InputText from '../components/InputText';
import { signUp } from '../utils/auth';
import { createUsers } from '../utils/databaseconn';
import {launchImageLibrary} from 'react-native-image-picker'

const SignUpMenu = ({navigation}) => {
  const[name, setName] = useState('')
  const[email, setEmail] = useState('')
  const[password, setPassword] = useState('')
  const[conpassword, setConPassword] = useState('')
  const[imageUri, setImageUri] = useState('')
  
  const handleOnSubmit = async () => {
        if(email != "" && password != "" && conpassword != ""&& imageUri != "" && name != ""){
        if(password == conpassword){
          signUp(email,password);
          createUsers(email,password,name,imageUri);
          // console.log(imageUri)
        }else{
          Alert.alert("Password did not match")
        }
      }else{
          Alert.alert("Form is not Complete")
      }
  };

  const selectImage = () => {
    launchImageLibrary(
      {
      mediaType:'photo',
      },
      ({assets}) => {
        if(assets && assets.length >0 ){
          setImageUri(assets[0].uri);
        }
      },
    );
  };

  return (
    <SafeAreaView>
      <Text style = {{
        height: 60,
        marginTop: 10,
        fontSize: 30,
        alignSelf:'center'
      }} >Sign Up</Text>

      {/* Image */}

      {
        imageUri == '' ? (
          <TouchableOpacity 
          style = {{
            alignSelf:'center',
            alignItems:'center',
            justifyContent:'center',
            padding:20,
            backgroundColor: "#8CDCDA",
          }} 
          onPress={selectImage}>
            <Text>+ Add Image</Text>
          </TouchableOpacity>
        ) : (
          <Image source={{
            uri: imageUri
          }}
          resizeMode={"center"}
          style={{
            width:"37.5%",
            height:150,
            borderRadius:100,
            alignSelf:'center'
          }}>

          </Image>
        )
      }

      {/* Nama */}
      <InputText 
      label="Name"
      placeholder='Enter Your Name'
      onChangeText={value => setName(value)}
      value={name}
      ></InputText>


      {/* Email */}
      <InputText 
      label="Email"
      placeholder='Enter Your Email'
      onChangeText={value => setEmail(value)}
      value={email}
      keyboardType={'email-address'}
      ></InputText>

      {/* password */}
      <InputText 
      label="Password"
      placeholder='Enter Your Password'
      onChangeText={value => setPassword(value)}
      value={password}
      secureTextEntry = {true}
      ></InputText>

      {/* password */}
      <InputText 
      label="Confirm Password"
      placeholder='Confirm Your Password'
      onChangeText={value => setConPassword(value)}
      value={conpassword}
      secureTextEntry = {true}
      ></InputText>


      <FormButton
      label='Register'
      handleonpress={handleOnSubmit}
      style={{
        width:'80%',
        alignSelf: 'center',
        marginTop: 20
      }}
      ></FormButton>

<Text style={styles.signup} onPress={()=>navigation.navigate('LoginMenu')}> Already Have Account? Log In</Text>
      

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  signup:{
    color: "red",
    alignSelf: 'center'
  }
});




export default SignUpMenu;