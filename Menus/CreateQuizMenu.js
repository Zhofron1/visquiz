import React,{useState,useEffect}  from 'react';
import {View,Text, TouchableOpacity, Alert} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import InputText from '../components/InputText';
import {launchImageLibrary} from 'react-native-image-picker'
import FormImage, { FormImageSelect } from '../components/FormImage';
import DropDownPicker from 'react-native-dropdown-picker';
import FormDropdown from '../components/FormDropdown';
import { createQuiz, getCategories } from '../utils/databaseconn';

const CreateQuizMenu = ({navigation}) => {
    useEffect(() => {
        initDropDown();
      }, [])
    
    const[imageUri,setImageUri] = useState('');
    const[author,setAuthor] = useState('');
    const[quizName,setQuizName] = useState('');
    const[diffRating,setDiffRating] = useState('');
    const [pickeropen, setOpen] = useState(false);
    const [picker, setValue] = useState(null); // Single
    const [idpicker, setIdPicker] = useState(''); // ID Category
    const [pickeritem, setItems] = useState([
        {label: 'Apple', value: 'apple'},
        {label: 'Banana', value: 'banana'}
      ]);

    const selectImage = () => {
        launchImageLibrary(
          {
          mediaType:'photo',
          },
          ({assets}) => {
            if(assets && assets.length >0 ){
              setImageUri(assets[0].uri);
            }
          },
        );
      };
    
    const handleOnSubmit = async () => {
        if(imageUri != '' && author != '' && quizName != '' && diffRating != '' && idpicker != null){
            createQuiz(author,idpicker,imageUri,quizName,diffRating)
            Alert.alert('Quiz Berhasil Dibuat')
            navigation.navigate("QuizHomeScreen")
        }else{
            // console.log('ID Picker = ' + idpicker)
            console.log(author)
            console.log(imageUri)
            console.log(quizName)
            console.log(diffRating)
            console.log(idpicker)
            Alert.alert('Data Tidak Komplit')
        }
    }

    const emptyFunction = () => {
        // console.log(idpicker)
    }

    const initDropDown = async () => {

        const categories = await getCategories();

        let tempcategory = []

        await categories.docs.forEach(async res => {
            let category = res.data()
            tempcategory.push({label: category.category_name,value: res.id})
        })
        setItems(tempcategory)
    }   
  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <Text style = {{
        height: 60,
        marginTop: 10,
        fontSize: 30,
        alignSelf:'center',
        color: "#000000",
      }} >Create Quiz</Text>
    
    {/* Image */}

    {
        imageUri == '' ? <FormImageSelect handleonpress={selectImage}></FormImageSelect> : <FormImage imageUri={imageUri}></FormImage>
      }
      {/* End Image */}

    <InputText 
      label="Author"
      placeholder='Enter Your Name'
      onChangeText={value => setAuthor(value)}
      value={author}
      keyboardType={'default'}
      ></InputText>

    <InputText 
      label="Quiz Name"
      placeholder='Enter The Quiz Name'
      onChangeText={value => setQuizName(value)}
      value={quizName}
      keyboardType={'default'}
      ></InputText>

    <FormDropdown
    label = "Quiz Category"
    onChangeValue = {value => setIdPicker(value)}
    onSelectItem = {emptyFunction}
    onPress = {emptyFunction}
    open = {pickeropen}
    value = {picker}
    items = {pickeritem}
    setOpen = {setOpen}
    setValue = {setValue}
    setItems = {setItems}
    >

    </FormDropdown>
    

    <InputText 
      label="Quiz Difficulty Rating"
      placeholder='1-5 (1 Sangat Mudah - 5 Sangat Sulit)'
      onChangeText={value => setDiffRating(value)}
      value={diffRating}
      keyboardType={'default'}
      ></InputText>
      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.backbutton}
        onPress={()=>{
          navigation.navigate('QuizHomeScreen')
        }}>
          <Text style={styles.backtext}>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.submitbutton}
        onPress={handleOnSubmit}>
          <Text style={styles.submittext}>Submit</Text>
        </TouchableOpacity>
      </View>
    
    </SafeAreaView>
    
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginVertical:"16%",
      color:"#FFFFFF",
    },
    mediumtext:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"24%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
    //   marginLeft: 20,
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
    //   alignSelf: "flex-start",
      // height:600,
    //   marginLeft:35,
      
    },
    submitbutton:{
        backgroundColor: "#1E38C5",
        width: 150 ,
        height: 50,
        marginTop:30,
        marginLeft:30,
        borderRadius:10,
    
    },
    backbutton:{
        backgroundColor: "#FFFFFF",
        width: 150 ,
        height: 50,
        marginTop:30,
        marginLeft:30,
        borderRadius:10,
    
    },
    backtext:{
        fontSize:18,
      alignSelf:"center",
      marginTop:10,
      textAlign:"center",
      color:"#B60000",
    },
    submittext:{
        fontSize:18,
      alignSelf:"center",
      marginTop:10,
      textAlign:"center",
      color:"#FFFFFF",
    }
  });

export default CreateQuizMenu;