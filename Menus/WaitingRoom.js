import React,{useEffect,useState} from 'react';
import {View,Text, TouchableOpacity, FlatList} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuizView from '../components/QuizView';
import { addPlayerToPlayRoom, getAllPlayers, getAllQuiz, getPlayerByEmail, getStatusHostFromDatabase, getStatusQuizFromDatabase, startQuizByID } from '../utils/databaseconn';
import firestore from '@react-native-firebase/firestore';
import QuestionView from '../components/QuestionView';
import PlayerView from '../components/PlayerView';


const WaitingRoom = ({navigation,route}) => {

  useEffect(() => {
    // initDropDown();
    getPlayers()
    // console.log(route.params.playerEmail)
    playerSnapshot(route.params.quizID)
    playRoomSnapShot(route.params.quizID)
    // console.log("Test");
    var id = window.setTimeout(function() {}, 0);

                        while (id--) {
                            window.clearTimeout(id); // will do nothing if no timeout with id is present
                        }

    
    
    checkStatus()
    
  }, [])

  const getStatusHost = async () => {
    let data = await getPlayerByEmail(route.params.playerEmail)
    let status = await getStatusHostFromDatabase(route.params.quizID,data.id)
    setStatusHost(status);

  }

  const checkStatus = async() => {
    const statusQuiz = await getStatusQuizFromDatabase(route.params.quizID)
    
    // console.log(statusQuiz);
    if(statusQuiz == true){
      navigation.navigate("PlayScreen",{quizID: route.params.quizID})
    }else{
      getStatusHost(route.params.playerEmail)
      addPlayer()
    }
  
  }

  const [playersArrays, setPlayerArray] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [statusHost, setStatusHost] = useState(false);
  // const [statusQuiz, setStatusQuiz] = useState(false);

  const addPlayer = async () =>{
    // console.log("Its Pressed")
    let data = await getPlayerByEmail(route.params.playerEmail)

    addPlayerToPlayRoom(route.params.quizID,data.id,data.name,data.picturi)
  }

  // const unsub = db.collection('cities').onSnapshot(() => {

  // });

  

  const playerSnapshot = (quizID) => {
    const doc = firestore().collection('PlayRoom').doc(quizID).collection("Players");
    // console.log(doc);
    // console.log("It does Start It");

    const observer = doc.onSnapshot(docSnapshot => {
      // console.log(`Received Player snapshot: ${docSnapshot}`);
      getPlayers()
      checkStatus()
    }, err => {
      // console.log(`Encountered error: ${err}`);
    });
    // observer();
    observer.apply
    // observer.call()
    return;

    

  }

  const playRoomSnapShot = (quizID) => {
    const doc = firestore().collection('PlayRoom').doc(quizID);
    // console.log(doc);
    // console.log("It does Start It");

    const observer = doc.onSnapshot(docSnapshot => {
      // console.log(`Received PlayRoom snapshot: ${docSnapshot}`);
      // getPlayers()
      checkStatus()

    }, err => {
      // console.log(`Encountered error: ${err}`);
    });
    // observer();
    observer.apply
    // observer.call()
    return;

    

  }

 
  
  // ...
  
  // Stop listening for changes
  // unsub();

  const getPlayers = async () =>{
    setRefreshing(true);
    const players = await getAllPlayers(route.params.quizID);

    let tempplayer = [];

    await players.docs.forEach(async res => {
        let player = res.data();
        tempplayer.push({
          playername: player.playerName,
          thumbnailuri: player.playerPicturi,
          
        });

        // console.log(tempplayer);
    })
    // setItems(tempquiz)
    // console.log(tempquiz[0]["quizname"])
    // console.log(tempquiz[1]["quizname"])
    setPlayerArray(tempplayer);
    setRefreshing(false);
  }

  // let quizarray = []

  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>

        {
          statusHost == true ? <TouchableOpacity style={styles.playbutton}
          onPress={()=>{
            // navigation.navigate('CreateQuizMenu')
            startQuizByID(route.params.quizID);
            navigation.navigate("PlayScreen",{quizID: route.params.quizID})
          }}>
            <Text style={styles.bigtext}>Start Quiz</Text>
          </TouchableOpacity> :
          <Text></Text>
        }
        
        

      </View>
      <Text style={styles.mediumtext}>Players Active</Text>


        <FlatList
        data={playersArrays}
        onRefresh={getPlayers}
        refreshing={refreshing}
        renderItem={({item: player}) => (
          <PlayerView
            playername={player.playername}
            thumbnailuri={player.thumbnailuri}>
          </PlayerView> 
        )}
      />
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginTop:"1%",
      color:"#FFFFFF",
      fontFamily:"times"
    },
    mediumtext:{
      fontSize:30,
    //   alignSelf:"center",
      marginLeft:20,
      marginTop:20,
    //   textAlign:"center",
      color:"#000000",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 50,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
      // alignSelf: "flex-start",
      // height:600,
      
    }
  });

export default WaitingRoom;