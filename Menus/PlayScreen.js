import React,{useEffect,useState, useRef} from 'react';
import {View,Text, TouchableOpacity, FlatList, Alert} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuizView from '../components/QuizView';
import { addPlayerToPlayRoom, getAllPlayers, getAllQuestion, getAllQuiz, getPlayerByEmail, getQuestionByQuizID, getStatusDoneFromDatabase, getStatusHostFromDatabase, startQuizByID, updateScore } from '../utils/databaseconn';
import firestore from '@react-native-firebase/firestore';
import QuestionView from '../components/QuestionView';
import PlayerView from '../components/PlayerView';
import PlayQuizView from '../components/PlayQuizView';
import arrayShuffle from 'array-shuffle';
import auth from '@react-native-firebase/auth';


const PlayScreen = ({navigation,route}) => {
    let currentQuestionPosition = 1;
    let cumulativeTime = 0;
    const selectionref = useRef('')
    const currentQuestionRef = useRef('')
    const scoreRef = useRef(0)
    const cumulativeTimeRef = useRef(0)

    useEffect(() => {
        
    currentQuestionPosition = 1;
    getQuestions().then(() => {
        cumulativeTimeRef.current = 0;
        for (let index = 0; index < questionArray.length; index++) {
            if(index >= questionArray.length){
              var id = window.setTimeout(function() {}, 0);

                        while (id--) {
                            window.clearTimeout(id); // will do nothing if no timeout with id is present
                        }
                navigation.navigate("LeaderboardScreen",{quizID: route.params.quizID})
                
                    
            }else{
            cumulativeTimeRef.current += questionArray[index].time *1000
            setTimeout(() => {
                checkQuestion()
            }, cumulativeTimeRef.current+ (3000*(index)));
            
            setTimeout(() => {
                if(index >= questionArray.length-1){
                  var id = window.setTimeout(function() {}, 0);

                  while (id--) {
                      window.clearTimeout(id); // will do nothing if no timeout with id is present
                  }
                    updateScore(route.params.quizID,auth().currentUser.email,parseInt(scoreRef.current))
                    navigation.navigate("LeaderboardScreen",{quizID: route.params.quizID})
                    
                    
                }else{
                nextQuestion()
                }
            }, (cumulativeTimeRef.current + (3000*(index+1))));
            
            // console.log(cumulativeTimeRef.current+ (3000*(index)));
            // console.log(cumulativeTimeRef.current+ (3000*(index+1)));
            }
            
        }
        checkStatusDonePlayer()
        

        
    }

    )
    
    
    


    }, [currentSelection,currentQuestion,score,cumulativeTime])

//   const [questionArray, setQuestionArray] = useState([]);
    let questionArray = [];
    const [currentQuestion2, setCurrentQuestion] = useState([]);
    let currentQuestion = [];
    let currentSelection = '';
    let score = 0;
  
//   const [currentSelection, setCurrentSelection] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const [timer, setTimer] = useState(0);

    const updateQuestions = ()=>{
    // console.log("Called Question : "+(currentQuestionPosition-1));
    if(questionArray == ''){
        console.log("Somehow Empty");
    }else{  
        updateCurrentQuestion(questionArray[(currentQuestionPosition-1)])
        // setCurrentQuestion(questionArray[(currentQuestionPosition-1)]);
        // currentQuestionRef.current = questionArray[(currentQuestionPosition-1)]
    }

    

  }

  const checkStatusDonePlayer = async () => {
    let playerid = await getPlayerByEmail(auth().currentUser.email)
    
    let data = await getStatusDoneFromDatabase(route.params.quizID,playerid.id)
    // console.log("Somehow not In");
    if(data.statusDone == true){
        // console.log("Done");
        var id = window.setTimeout(function() {}, 0);

        while (id--) {
            window.clearTimeout(id); // will do nothing if no timeout with id is present
        }
        navigation.navigate("LeaderboardScreen",{quizID: route.params.quizID})
        
    }

    
}

  const resetQuestionSession = () =>{
    selectionref.current = ''
    currentQuestionRef.current = []
  }

  const getQuestions = async () =>{
    setRefreshing(true);
    // console.log(route.params.quizID);
    const quizzes = await getQuestionByQuizID(route.params.quizID);
    
    let tempquestion = [];
    let indexnum = 0;

    tempquestion = await getAllQuestion(route.params.quizID)
    
    tempquestion = arrayShuffle(tempquestion)

    questionArray = tempquestion;
    updateQuestions()
    
    
    setRefreshing(false);
  }

  const checkQuestion = () => {
    // const selection = useRef(currentSelection)
    // selection.current = currentSelection
    setRefreshing(true);
    let data = currentQuestionRef.current
    if(data.correct_answer == selectionref.current){
        // console.log("Jawaban Benar");
        Alert.alert("Jawaban Anda Benar")
        scoreRef.current = parseInt(scoreRef.current) - 0 + parseInt(currentQuestionRef.current.score) - 0
        // scoreRef.current = score
        // console.log("Test " + currentQuestionRef.current.score);
        // console.log("Score " + scoreRef.current);
    }else{
        // console.log("Jawaban Salah");
        Alert.alert("Jawaban Anda Salah")
    }
    setRefreshing(false);
    
    
    
  }

  const nextQuestion = () => {
    currentQuestionPosition ++;
    resetQuestionSession()
    updateQuestions()
  }

  const updateCurrentQuestion = (tempquestion) =>{
    let tempcurrquestion = tempquestion;
    // console.log("Test");
    // console.log(tempquestion);
    let tempcurroption = [
        tempquestion.option_1,
        tempquestion.option_2,
        tempquestion.option_3,
        tempquestion.option_4,
    ]
    tempcurroption = arrayShuffle(tempcurroption)
    tempcurrquestion.option_1 = tempcurroption[0]
    tempcurrquestion.option_2 = tempcurroption[1]
    tempcurrquestion.option_3 = tempcurroption[2]
    tempcurrquestion.option_4 = tempcurroption[3]
    let tempcurrquest = [];
    tempcurrquest.push(tempcurrquestion)
    // console.log(tempcurrquestion);
    currentQuestion.push(tempcurrquestion);
    setCurrentQuestion(tempcurrquest);
    currentQuestionRef.current = tempcurrquestion
    // console.log("AAAA" + tempcurrquestion.option_3);
    // console.log("BBBB" + tempcurrquestion);
  }



  // let quizarray = []

  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>
        
        

      </View>
      


        <FlatList
        data={currentQuestion2}
        onRefresh={updateQuestions}
        refreshing={refreshing}
        renderItem={({item: question}) => (
            <View>
                <Text style={styles.mediumtext}>Score : {scoreRef.current}</Text>
            
          <PlayQuizView
            question = {question.question}
            option1 = {question.option_1}
            option2 = {question.option_2}
            option3 = {question.option_3}
            option4 = {question.option_4}
            thumbnailuri = {question.thumbnailuri}
            value = {question.score}
            handleOnPress = {(value) => {
                
                currentSelection = value
                selectionref.current = currentSelection.substring(3)
                // let data = value
                // console.log(currentSelection.substring(3));
                // console.log("Pressed Boy");
            }}
          >
            

          </PlayQuizView>
          </View>
        )}
      />
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginTop:"1%",
      color:"#FFFFFF",
      fontFamily:"times"
    },
    mediumtext:{
      fontSize:30,
    //   alignSelf:"center",
      marginLeft:20,
      marginTop:20,
    //   textAlign:"center",
      color:"#000000",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 50,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
      // alignSelf: "flex-start",
      // height:600,
      
    }
  });

export default PlayScreen;