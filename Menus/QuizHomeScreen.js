import React,{useEffect,useState} from 'react';
import {View,Text, TouchableOpacity, FlatList} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuizView from '../components/QuizView';
import { getAllQuiz } from '../utils/databaseconn';


const QuizHomeScreen = ({navigation}) => {

  useEffect(() => {
    // initDropDown();
    getQuizzes()
  }, [])

  const [quizArray, setQuizArray] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const emptyFunction = () =>{
    console.log("Its Pressed")
  }

  const getQuizzes = async () =>{
    setRefreshing(true);
    const quizzes = await getAllQuiz();

    let tempquiz = [];

    await quizzes.docs.forEach(async res => {
        let quiz = res.data();
        tempquiz.push({quizname: quiz.quiz_name,starrating: quiz.rating,thumbnailuri: quiz.picturi, quizid: res.id});
    })
    // setItems(tempquiz)
    // console.log(tempquiz[0]["quizname"])
    // console.log(tempquiz[1]["quizname"])
    setQuizArray(tempquiz);
    setRefreshing(false);
  }

  // let quizarray = []

  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.playbutton}
        onPress={()=>{
          navigation.navigate('CreateQuizMenu')
        }}>
          <Text style={styles.bigtext}>Create New Quiz</Text>
        </TouchableOpacity>
        

      </View>
      <Text style={styles.mediumtext}>Available Quizzes</Text>


        <FlatList
        data={quizArray}
        onRefresh={getQuizzes}
        refreshing={refreshing}
        renderItem={({item: quiz}) => (
          <QuizView
            quizname={quiz.quizname}
            starrating={quiz.starrating}
            thumbnailuri={quiz.thumbnailuri}
            onPress={()=>{
              navigation.navigate('QuestionHomeMenu',{quizID: quiz.quizid})
            }}
            type={"Edit"}>
          </QuizView> 
        )}
      />
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginTop:"1%",
      color:"#FFFFFF",
      fontFamily:"times"
    },
    mediumtext:{
      fontSize:30,
    //   alignSelf:"center",
      marginLeft:20,
      marginTop:20,
    //   textAlign:"center",
      color:"#000000",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 50,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
      // alignSelf: "flex-start",
      // height:600,
      
    }
  });

export default QuizHomeScreen;