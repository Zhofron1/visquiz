
import React from 'react';
import {View,Text, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.playbutton}
        onPress={()=>{
          navigation.navigate('PlayQuizMenu')
        }}>
          <Text style={styles.bigtext}> Play Quiz</Text>
        </TouchableOpacity>

      </View>
      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.smallbutton}
        onPress={()=>{
          navigation.navigate('QuizHomeScreen')
        }}>
          <Text style={styles.mediumtext}>Quiz Menus</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.smallbutton}
        onPress={()=>{
          signOut()
        }}>
          <Text style={styles.mediumtext2}>Logout</Text>
        </TouchableOpacity>
      </View>
        
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
  bigtext:{
    fontSize:30,
    alignSelf:'center',
    marginVertical:"16%",
    color:"#FFFFFF",
  },
  mediumtext:{
    fontSize:30,
    alignSelf:"center",
    marginVertical:"24%",
    textAlign:"center",
    color:"#FFFFFF",

  },
  mediumtext2:{
    fontSize:30,
    alignSelf:"center",
    marginVertical:"33%",
    textAlign:"center",
    color:"#FFFFFF",

  },
  smallbutton:{
    backgroundColor: "#1E38C5",
    width: 150 ,
    height: 150,
    marginTop:30,
    marginLeft:30,
    borderRadius:10,

  },
  playbutton:{
    backgroundColor: "#1E38C5",
    width: 330 ,
    height: 150,
    marginTop:30,
    marginLeft:30,
    borderRadius:10,

  },
  horizontalcontainer:{
    flexDirection: 'row',
    // backgroundColor: "#00FF00",
    // height:200,
  },
  verticalcontainer:{
    // backgroundColor: "#0000FF",
    flexDirection: "column",
    alignSelf: "flex-start",
    // height:600,
    
  }
});

export default HomeScreen;