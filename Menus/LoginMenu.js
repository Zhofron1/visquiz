import React, { useState } from 'react';
import {View, Text, Image, ScrollView, TextInput, SafeAreaView, Button} from 'react-native';
import {StyleSheet} from 'react-native';
import FormButton from '../components/FormButton';
import InputText from '../components/InputText';
import { Login } from '../utils/auth';

const LoginMenu = ({navigation}) => {
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')

    const handeOnSubmit = () => {
        if(email != "" && password != ""){
            Login(email,password)
        }
    };

  return (
    <SafeAreaView>
      <Text style = {{
        height: 60,
        marginTop: 10,
        fontSize: 30,
        alignSelf:'center'
      }} >Login</Text>

      {/* Email */}
      <InputText 
      label="Email"
      placeholder='Enter Your Email'
      onChangeText={value => setEmail(value)}
      value={email}
      keyboardType={'email-address'}
      ></InputText>

      {/* password */}
      <InputText 
      label="Password"
      placeholder='Enter Your Password'
      onChangeText={value => setPassword(value)}
      value={password}
      secureTextEntry = {true}
      ></InputText>

      <FormButton
      label='Login'
      handleonpress={handeOnSubmit} 
      style={{
        width:'80%',
        alignSelf: 'center',
        marginTop: 20
      }}
      ></FormButton>

    <Text style={styles.signup} onPress={()=>navigation.navigate('SignUpMenu')}> Don't have account? Sign Up</Text>
      
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    signup:{
      color: "red",
      alignSelf: 'center'
    }
  });




export default LoginMenu;