import React,{useEffect,useState} from 'react';
import {View,Text, TouchableOpacity, FlatList} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuizView from '../components/QuizView';
import { deleteQuestionByID, getAllQuiz, getQuestionByQuizID, getQuizNameByQuizID } from '../utils/databaseconn';
import QuestionView from '../components/QuestionView';


const QuestionHomeMenu = ({navigation,route}) => {
    let quizName= ""
    useEffect(() => {
    // initDropDown();
    initQuizName()
    getQuestions()
    // initQuizName()
    
  }, [])

  const [questionArray, setQuestionArray] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const initQuizName = async() => {
    let name = await getQuizNameByQuizID(route.params.quizID);
    quizName = name
  }

  const deleteQuestion = (questionID) =>{

    deleteQuestionByID(route.params.quizID,questionID).then(() => {
        console.log("Delete Success")
    }
    )
  }
  const emptyFunction = () =>{
    console.log("Its Pressed")
  }

  const getQuestions = async () =>{
    setRefreshing(true);

    const quizzes = await getQuestionByQuizID(route.params.quizID);
    
    let tempquestion = [];
    let indexnum = 0;
    await quizzes.docs.forEach(async res => {
        indexnum++;
        let questiondata = res.data();
        tempquestion.push({
            question: questiondata.question,
            quizname: quizName,
            index: indexnum,  
            thumbnailuri: questiondata.attachment_uri,
            option_1: questiondata.option_1,
            option_2: questiondata.option_2,
            option_3: questiondata.option_3,
            option_4: questiondata.option_4,
            correct_answer: questiondata.correct_answer,
            score: questiondata.score,
            time: questiondata.time,
            questionid: res.id
        });
        // console.log(questiondata.attachment_uri)
    })
    // setItems(tempquiz)
    // console.log(tempquiz[0]["quizname"])
    // console.log(tempquiz[1]["quizname"])
    setQuestionArray(tempquestion);
    
    setRefreshing(false);
  }

  // let quizarray = []

  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.playbutton}
        onPress={()=>{
          navigation.navigate('CreateQuestionMenu',{quizID: route.params.quizID})
        }}>
          <Text style={styles.bigtext}>Add Question</Text>
        </TouchableOpacity>
        

      </View>
      <Text style={styles.mediumtext}>Quiz Questions</Text>
        <FlatList
        data={questionArray}
        onRefresh={getQuestions}
        refreshing={refreshing}
        renderItem={({item: question}) => (
          <QuestionView
            questionnumber={question.index+". " + question.question}
            quizname={question.quizname}
            thumbnailuri={question.thumbnailuri}
            imconfused={() => {
                deleteQuestion(question.questionid)
                getQuestions()
                // console.log("Refreshed");
            }
            }
            type={"Edit"}>
          </QuestionView> 
        )}
      />
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginTop:"1%",
      color:"#FFFFFF",
      fontFamily:"times"
    },
    mediumtext:{
      fontSize:30,
    //   alignSelf:"center",
      marginLeft:20,
      marginTop:20,
    //   textAlign:"center",
      color:"#000000",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 50,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
      // alignSelf: "flex-start",
      // height:600,
      
    }
  });

export default QuestionHomeMenu;