import React,{useState,useEffect}  from 'react';
import {View,Text, TouchableOpacity, Alert} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import InputText from '../components/InputText';
import {launchImageLibrary} from 'react-native-image-picker'
import FormImage, { FormImageSelect } from '../components/FormImage';
import DropDownPicker from 'react-native-dropdown-picker';
import FormDropdown from '../components/FormDropdown';
import { createQuestion, createQuiz, getCategories, getQuizNameByQuizID } from '../utils/databaseconn';
import { ScrollView } from 'react-native-gesture-handler';


const CreateQuestionMenu = ({navigation,route}) => {
    useEffect(() => {
        // initDropDown();
        initQuizTitle();
      }, [])
    
      const[quizTitle,setQuizTitle] = useState('');
      const[question,setQuestion] = useState('');
      const[correctAnswer,setCorrectAnswer] = useState('');
      const[option2,setOption2] = useState('');
      const[option3,setOption3] = useState('');
      const[option4,setOption4] = useState('');
      const[score,setScore] = useState('');
      const[time,setTime] = useState('');

      const[imageUri,setImageUri] = useState('');





    const initQuizTitle = async () => {
      console.log(route.params.quizID)
      let name = await getQuizNameByQuizID(route.params.quizID);
      console.log(name)
      // quizTitle = name;
      setQuizTitle(name)
    } 

    const selectImage = () => {
        launchImageLibrary(
          {
          mediaType:'photo',
          },
          ({assets}) => {
            if(assets && assets.length >0 ){
              setImageUri(assets[0].uri);
            }
          },
        );
      };
    
    const handleOnSubmit = async () => {
        if(imageUri != '' && question != '' && correctAnswer != '' && option2 != '' && option3 != '' && option4 != '' && score != '' && time != ''){
            // createQuiz(author,idpicker,imageUri,quizName,diffRating)
            await createQuestion(route.params.quizID,imageUri,correctAnswer,option2,option3,option4,question,score,time).then(() => {
              Alert.alert('Pertanyaan Berhasil Ditambah')
              navigation.navigate("QuestionHomeMenu",{quizID: route.params.quizID})
            }
            )
            // Alert.alert('Question Berhasil Dibuat')
            
        }else{
            Alert.alert('Data Tidak Komplit')
        }
    }

    const emptyFunction = () => {
        // console.log(idpicker)
    }

  return (
    <ScrollView style={styles.verticalcontainer}>
      <Text style = {{
        height: 40,
        marginTop: 10,
        fontSize: 30,
        alignSelf:'center',
        color: "#000000",
      }} >Add Question</Text>
      <Text style = {{
        height: 30,
        // marginTop: 10,
        fontSize: 18,
        alignSelf:'center',
        color: "#000000",
      }} >{quizTitle}</Text>
    
    {/* Image */}

    {
        imageUri == '' ? <FormImageSelect handleonpress={selectImage}></FormImageSelect> : <FormImage imageUri={imageUri}></FormImage>
      }
      {/* End Image */}

    <InputText 
      label="Question"
      placeholder='Enter The Question'
      onChangeText={value => setQuestion(value)}
      value={question}
      keyboardType={'default'}
      ></InputText>

    <InputText 
      label="Correct Option"
      placeholder='Enter The Answer'
      onChangeText={value => setCorrectAnswer(value)}
      value={correctAnswer}
      keyboardType={'default'}
      ></InputText>

    <InputText 
      label="Option 2"
      placeholder='Enter Option 2'
      onChangeText={value => setOption2(value)}
      value={option2}
      keyboardType={'default'}
      ></InputText>

    <InputText 
      label="Option 3"
      placeholder='Enter Option 3'
      onChangeText={value => setOption3(value)}
      value={option3}
      keyboardType={'default'}
      ></InputText>

    <InputText 
      label="Option 4"
      placeholder='Enter Option 4'
      onChangeText={value => setOption4(value)}
      value={option4}
      keyboardType={'default'}
      ></InputText>
    
    <InputText 
      label="Score"
      placeholder='Enter the Question Score'
      onChangeText={value => setScore(value)}
      value={score}
      keyboardType={'numeric'}
      ></InputText>

<InputText 
      label="Time (Seconds)"
      placeholder='Enter Question Duration'
      onChangeText={value => setTime(value)}
      value={time}
      keyboardType={'numeric'}
      ></InputText>
    


      <View style={styles.horizontalcontainer}>
        <TouchableOpacity style={styles.backbutton}
        onPress={()=>{
          navigation.navigate('QuestionHomeMenu', {quizID : route.params.quizID})
        }}>
          <Text style={styles.backtext}>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.submitbutton}
        onPress={handleOnSubmit}>
          <Text style={styles.submittext}>Submit</Text>
        </TouchableOpacity>
      </View>
    
    </ScrollView>
    
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginVertical:"16%",
      color:"#FFFFFF",
    },
    mediumtext:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"24%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
    //   marginLeft: 20,
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
    //   alignSelf: "flex-start",
      // height:600,
    //   marginLeft:35,
      
    },
    submitbutton:{
        backgroundColor: "#1E38C5",
        width: 150 ,
        height: 50,
        marginTop:30,
        marginLeft:30,
        borderRadius:10,
    
    },
    backbutton:{
        backgroundColor: "#FFFFFF",
        width: 150 ,
        height: 50,
        marginTop:30,
        marginLeft:30,
        borderRadius:10,
    
    },
    backtext:{
        fontSize:18,
      alignSelf:"center",
      marginTop:10,
      textAlign:"center",
      color:"#B60000",
    },
    submittext:{
        fontSize:18,
      alignSelf:"center",
      marginTop:10,
      textAlign:"center",
      color:"#FFFFFF",
    }
  });

export default CreateQuestionMenu;