import React,{useEffect,useState} from 'react';
import {View,Text, TouchableOpacity, FlatList} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import { signOut } from '../utils/auth';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuizView from '../components/QuizView';
import { addPlayerToPlayRoom, getAllPlayers, getAllQuiz, getPlayerByEmail, getStatusHostFromDatabase, getStatusQuizFromDatabase, startQuizByID } from '../utils/databaseconn';
import firestore from '@react-native-firebase/firestore';
import QuestionView from '../components/QuestionView';
import PlayerView from '../components/PlayerView';
import PlayQuizMenu from './PlayQuizMenu';


const LeaderboardScreen = ({navigation,route}) => {

  useEffect(() => {
    // initDropDown();
    var id = window.setTimeout(function() {}, 0);

                        while (id--) {
                            window.clearTimeout(id); // will do nothing if no timeout with id is present
                        }
    getPlayers()
    // console.log(route.params.playerEmail)
    playerSnapshot(route.params.quizID)

    
  }, [])

  const [playersArrays, setPlayerArray] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  

  const playerSnapshot = (quizID) => {
    const doc = firestore().collection('PlayRoom').doc(quizID).collection("Players");
    // console.log(doc);
    console.log("It does Start It");

    const obs = doc.onSnapshot(docSnapshot => {
    //   console.log(`Received Player snapshot: ${docSnapshot}`);
      getPlayers()
    }, err => {
    //   console.log(`Encountered error: ${err}`);
    });
    // observer();
    obs.apply
    // observer.call()
    return;

    

  }
 
  
  // ...
  
  // Stop listening for changes
  // unsub();

  const getPlayers = async () =>{
    setRefreshing(true);
    const players = await getAllPlayers(route.params.quizID);

    let tempplayer = [];

    await players.docs.forEach(async res => {
        let player = res.data();
        if(player.statusDone == true){
            tempplayer.push({
                playername: player.playerName,
                thumbnailuri: player.playerPicturi,
                score: player.score
                
            });
        }
        

        // console.log(tempplayer);
    })
    // setItems(tempquiz)
    // console.log(tempquiz[0]["quizname"])
    // console.log(tempquiz[1]["quizname"])
    setPlayerArray(tempplayer);
    setRefreshing(false);
  }

  // let quizarray = []

  return (
    <SafeAreaView style={styles.verticalcontainer}>
      <View style={styles.horizontalcontainer}>

        
        

      </View>
      <Text style={styles.mediumtext}>Top Players</Text>


        <FlatList
        data={playersArrays}
        onRefresh={getPlayers}
        refreshing={refreshing}
        renderItem={({item: player}) => (
          <PlayerView
            playername={player.playername}
            thumbnailuri={player.thumbnailuri}
            score={player.score}>
          </PlayerView> 
        )}
      />
      <TouchableOpacity
      style={{
        position:'absolute',
        width:200,
        height:80,
        backgroundColor:"#7A8FFF",
        marginTop:680,
        alignSelf:'center',
        borderRadius:20,


      }}
      onPress={()=> {
        navigation.navigate('PlayQuizMenu')
      }}
      >
        <Text
        style={{
            fontSize:20,
            color:"#FFFFFF",
            alignSelf:'center',
            fontFamily:"times",
            marginTop:24,

        }}
        >Back To Quiz Menu</Text>

      </TouchableOpacity>
    </SafeAreaView>
    
  )
}

const styles = StyleSheet.create({
    bigtext:{
      fontSize:30,
      alignSelf:'center',
      marginTop:"1%",
      color:"#FFFFFF",
      fontFamily:"times"
    },
    mediumtext:{
      fontSize:30,
    //   alignSelf:"center",
      marginLeft:20,
      marginTop:20,
    //   textAlign:"center",
      color:"#000000",
  
    },
    mediumtext2:{
      fontSize:30,
      alignSelf:"center",
      marginVertical:"33%",
      textAlign:"center",
      color:"#FFFFFF",
  
    },
    smallbutton:{
      backgroundColor: "#1E38C5",
      width: 150 ,
      height: 150,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    playbutton:{
      backgroundColor: "#1E38C5",
      width: 330 ,
      height: 50,
      marginTop:30,
      marginLeft:30,
      borderRadius:10,
  
    },
    horizontalcontainer:{
      flexDirection: 'row',
      // backgroundColor: "#00FF00",
      // height:200,
    },
    verticalcontainer:{
      // backgroundColor: "#0000FF",
      flexDirection: "column",
      // alignSelf: "flex-start",
      // height:600,
      
    }
  });

export default LeaderboardScreen;