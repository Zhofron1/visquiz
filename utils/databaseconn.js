import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';


const usersCollection = firestore().collection('Users');


export const createUsers = async (email,password,name,picturl) => {
    const refdata = await firestore().collection('Users').add({
        email,password,name,picturl
    })
    const id = refdata.id;
    console.log(id);
    
    
    if(picturl != ""){
        const reference = storage().ref(`/images/users/${id}`);
        await reference.putFile(picturl).then(()=>{
            console.log("Success Image");
        });
        let picturi = await reference.getDownloadURL();
        email = email.toLowerCase();
        await firestore().collection('Users').doc(id).set({
            id,email,password,name,picturi
        });

    
    }

    
    
    return;
}

export const createQuiz = async (author, category_id, images_cover,quiz_name,rating) => {
    
    // add quiz
    const refdata = await firestore().collection('Quizzes').add({
        author,category_id,images_cover,quiz_name,rating
    })
    const id = refdata.id;
    const reference = storage().ref(`/images/quiz/${id}`);
    console.log(id);
    // add image to storage
    await reference.putFile(images_cover).then(()=>{
        console.log("Success Image");
    });
    // update quiz image link
    let picturi = await reference.getDownloadURL();
    await firestore().collection('Quizzes').doc(id).set({
        author,category_id,picturi,quiz_name,rating
    });
    return;
}

export const createQuestion = async (quizID,attachment_uri,correct_answer,option_2,option_3,option_4,question,score,time) => {
    
    let option_1 = correct_answer
    // add Question
    const refdata = await firestore().collection('Quizzes').doc(quizID).collection("Questions").add({
        attachment_uri,correct_answer,option_1,option_2,option_3,option_4,question,score,time
    })
    const id = refdata.id;
    const reference = storage().ref(`/images/questions/${id}`);
    // console.log(id);
    // add image to storage
    await reference.putFile(attachment_uri).then(()=>{
        console.log("Success Image");
    });
    // update quiz image link
    let picturi = await reference.getDownloadURL();
    attachment_uri = picturi
    await firestore().collection('Quizzes').doc(quizID).collection("Questions").doc(id).set({
        attachment_uri,correct_answer,option_1,option_2,option_3,option_4,question,score,time
    });
    return;
    // return;
}

export const getCategories = () => {
    return firestore().collection("Categories").get();
}

export const getAllQuiz = () => {
    return firestore().collection("Quizzes").get();
}

export const getAllPlayers = (quizID) => {
    return firestore().collection("PlayRoom").doc(quizID).collection("Players").get();
}

export const getQuestionByQuizID = (quizID) => {
    return firestore().collection("Quizzes").doc(quizID).collection("Questions").get();
}

export const getQuizNameByQuizID = async (quizID) => {
    let name = await firestore().collection("Quizzes").doc(quizID).get();
    name = name.data()
    // return firestore().collection("Quizzes").doc(quizID).get();
    return name.quiz_name;
}

export const getPlayerByEmail = async (playerEmail) => {
    const snapshot = await firestore().collection("Users").where("email" ,'==', playerEmail).get();
    let id_data = []
    if(snapshot.empty){
        console.log("Its Empty");
    }else{
        snapshot.forEach(data => {
            // console.log(data.data().id)
            id_data = data.data()
        });
        // data = snapshot.data()
    }
    return id_data;
}



export const deleteQuestionByID = async (quizID,questionID) => {
    // console.log("Berhasil " + quizID + " : " + questionID)
    return await firestore().collection('Quizzes').doc(quizID).collection("Questions").doc(questionID).delete();
    // return;
}

export const addPlayerToPlayRoom = async (quizID,playerID,playerName,playerPicturi) => {
    let data = await getStatusDoneFromDatabase(quizID,playerID)
    // console.log("Wut");
    // console.log(data);
    // console.log("Wut");
    if(typeof data == "undefined"){
        console.log("Its Checked");
    }else{
        // if(data.statusDone == true ){
            // return;
        // }
    }
    // console.log(quizID+playerID)
    
    let score= 0;
    let statusDone = false;
    await firestore().collection('PlayRoom').doc(quizID).collection("Players").doc(playerID).set({
        playerName,playerPicturi,score,statusDone
    })

    return;
}

export const getStatusHostFromDatabase = async (quizID,playerID) => {
    let statusdata = await firestore().collection('PlayRoom').doc(quizID).get();
    statusdata = statusdata.data();
    let status = false;
    if(statusdata.hostID == playerID){
        status = true;
    }
    return status;
}

export const getStatusQuizFromDatabase = async (quizID) => {
    let statusdata = await firestore().collection('PlayRoom').doc(quizID).get();
    statusdata = statusdata.data();
    let status = statusdata.status
    // console.log(status);
    return status;
}

export const getStatusDoneFromDatabase = async (quizID,playerID) => {
    let data = await firestore().collection('PlayRoom').doc(quizID).collection("Players").doc(playerID).get();
    data = data.data();
    // let status = statusdata.status
    // console.log(status);
    return data;
}

export const startQuizByID = async (quizID) => {
    await firestore().collection('PlayRoom').doc(quizID).update({status: true});
    return;
}

export const updateScore = async (quizID,playerEmail,playerScore) => {
    let data = await getPlayerByEmail(playerEmail)
    let score = parseInt(playerScore)
    await firestore().collection('PlayRoom').doc(quizID).collection("Players").doc(data.id).update({score: score, statusDone: true});
    return;
}

export const getAllQuestion = async (quizID) => {
    let indexnum = 0;
    let tempquestion = [];
    let tempres = "Multiple";

    let quizzes = await getQuestionByQuizID(quizID);
    quizzes.docs.forEach(async res => {
        indexnum++;
        let questiondata = res.data();
        if(tempres == "Multiple"){
            tempquestion.push({
                question: questiondata.question,
                index: indexnum,  
                thumbnailuri: questiondata.attachment_uri,
                option_1: questiondata.option_1,
                option_2: questiondata.option_2,
                option_3: questiondata.option_3,
                option_4: questiondata.option_4,
                correct_answer: questiondata.correct_answer,
                score: questiondata.score,
                time: questiondata.time,
                questionid: res.id
            });
            console.log("Added");
        }else if(questiondata.type == "Matching"){
            tempquestion.push({
                
                index: indexnum,  
                thumbnailuri_1: questiondata.attachment_uri_1,
                thumbnailuri_2: questiondata.attachment_uri_2,
                thumbnailuri_3: questiondata.attachment_uri_3,
                thumbnailuri_4: questiondata.attachment_uri_4,
                question_1: questiondata.question_1,
                question_2: questiondata.question_2,
                question_3: questiondata.question_3,
                question_4: questiondata.question_4,
                correct_answer_1: questiondata.correct_answer_1,
                correct_answer_2: questiondata.correct_answer_2,
                correct_answer_3: questiondata.correct_answer_3,
                correct_answer_4: questiondata.correct_answer_4,
                score: questiondata.score,
                time: questiondata.time,
                questionid: res.id
            });
        }else if(res.type == "TrueFalse"){
            tempquestion.push({
                question: questiondata.question,
                index: indexnum,  
                thumbnailuri: questiondata.attachment_uri,
                correct_answer: questiondata.correct_answer,
                score: questiondata.score,
                time: questiondata.time,
                questionid: res.id
            });
        }else if(res.type == "Video"){
            tempquestion.push({
                question: questiondata.question_video_link,
                index: indexnum,  
                thumbnailuri: questiondata.attachment_uri,
                option_1: questiondata.option_1,
                option_2: questiondata.option_2,
                option_3: questiondata.option_3,
                option_4: questiondata.option_4,
                correct_answer: questiondata.correct_answer,
                score: questiondata.score,
                time: questiondata.time,
                questionid: res.id
            });
        }else if(res.type == "Audio"){
            tempquestion.push({
                question: questiondata.question_audio_link,
                index: indexnum,  
                thumbnailuri: questiondata.attachment_uri,
                option_1: questiondata.option_1,
                option_2: questiondata.option_2,
                option_3: questiondata.option_3,
                option_4: questiondata.option_4,
                correct_answer: questiondata.correct_answer,
                score: questiondata.score,
                time: questiondata.time,
                questionid: res.id
            });
        }
        
        // console.log(questiondata.attachment_uri)
    })
    return tempquestion;
}
