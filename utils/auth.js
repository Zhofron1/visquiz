import auth from '@react-native-firebase/auth';
import {ToastAndroid,Alert} from 'react-native';


export const Login = (email,password) => {
    auth()
        .signInWithEmailAndPassword(email,password)
        .then(() => {
            console.log('User account Logged in!');
            ToastAndroid.show("Logged In",ToastAndroid.SHORT)
            Alert.alert("User account Logged in!")
        })
        .catch(err => {

            console.log(err);
            if(err == "[Error: [auth/user-not-found] There is no user record corresponding to this identifier. The user may have been deleted.]"){
                Alert.alert("Identitas Salah")
            }
        })
}

export const signUp = (email,password) => {
    auth()
        .createUserWithEmailAndPassword(email,password)
        .then(() => {
            console.log('User account Created!');
            ToastAndroid.show("Signed Up",ToastAndroid.SHORT)
            Alert.alert("User account Created!")
        })
        .catch(err => {
            console.log(err);
        })
}

export const signOut = () => {
    auth().signOut().then(() => {
        ToastAndroid.show("Signed Out",ToastAndroid.SHORT);
        Alert.alert("Signed Out Succesfully");
    })
} 