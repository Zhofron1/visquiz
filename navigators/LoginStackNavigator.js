import { createStackNavigator } from '@react-navigation/stack';
import LoginMenu from '../Menus/LoginMenu';
import SignUpMenu from '../Menus/SignUpMenu';

const Stack = createStackNavigator();

function LoginStackNavigator() {
  return (
    <Stack.Navigator screenOptions={{
        headerShown:false,
        }}>
      
      <Stack.Screen name="LoginMenu" component={LoginMenu} />
      <Stack.Screen name="SignUpMenu" component={SignUpMenu} />
    </Stack.Navigator>
  );
}

export default LoginStackNavigator;