import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../Menus';
import CreateQuestionMenu from '../Menus/CreateQuestionMenu';
import CreateQuizMenu from '../Menus/CreateQuizMenu';
import PlayQuizMenu from '../Menus/PlayQuizMenu';
import QuestionHomeMenu from '../Menus/QuestionHomeMenu';
import QuizHomeScreen from '../Menus/QuizHomeScreen';
import WaitingRoom from '../Menus/WaitingRoom';
import PlayScreen from '../Menus/PlayScreen';
import LeaderboardScreen from '../Menus/LeaderboardScreen';

const Stack = createStackNavigator();

function HomeStackNavigator() {
  return (
    <Stack.Navigator screenOptions={{
        headerShown:false,
        }}>
      
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="CreateQuizMenu" component={CreateQuizMenu} />
      <Stack.Screen name="QuizHomeScreen" component={QuizHomeScreen} />
      <Stack.Screen name="QuestionHomeMenu" component={QuestionHomeMenu} />
      <Stack.Screen name="CreateQuestionMenu" component={CreateQuestionMenu} />
      <Stack.Screen name="PlayQuizMenu" component={PlayQuizMenu} />
      <Stack.Screen name="WaitingRoom" component={WaitingRoom} />
      <Stack.Screen name="PlayScreen" component={PlayScreen} />
      <Stack.Screen name="LeaderboardScreen" component={LeaderboardScreen} />


    </Stack.Navigator>
  );
}

export default HomeStackNavigator;