import React from 'react';
import { TextInput,View,Text } from 'react-native';
import {StyleSheet, Image} from 'react-native';
import FormImage from './FormImage';
import FormButton from '../components/FormButton';
import { TouchableOpacity } from 'react-native-gesture-handler';

const PlayerView = ({
    playername = 'Zhofron AFGP',
    thumbnailuri = 'https://firebasestorage.googleapis.com/v0/b/visquiz-6bdd5.appspot.com/o/images%2Fdatabase%2F2107957.png?alt=media&token=2b492933-82ca-4b14-a98c-9689c6026ae8',
    score = 0,
    onPress = null,
    ...more
}) => {
    return (
        <View style={styles.maincontainer}>
            <Image source={{ uri : thumbnailuri}}
            style={styles.image}
            >

            </Image>
            <View style={styles.verticalcontainer}>
                {/* <Text style={styles.label}>1. Siapa kah pembuat aplikasi quiz VISQUIZ?</Text> */}
                <Text style={styles.label}>{playername}</Text>
                {/* <View style={styles.horizontalcontainer}>
                    <Text style={styles.startext}>{quizname}</Text>
                </View> */}
                {
                    score == 0 ? 
                    <View></View>
                    :
                    <View style={styles.horizontalcontainer}>
                    <Text style={styles.smalltext}>{score}</Text>
                    </View> 
                }
                
                

                
            </View>
            
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        // alignSelf: 'center',
        borderWidth: 1,
        // padding: 10,
        // width: 320,
        marginTop: 0,
        
        backgroundColor: "#FFFFFF",
    },
    label: {
        marginTop:5,
            height: 40,
            // marginLeft: 40,
            marginTop:10,
            marginLeft:5,
            width:230,
            fontSize: 32,
            color: "#FFFFFF",
            fontFamily:"times",
            // alignSelf:'center',
    },
    maincontainer:{
        width:350,
        backgroundColor:'#7A8FFF',
        height:100,
        alignSelf:'center',
        borderRadius:10,
        flexDirection:'row',
        marginTop:15,

    },
    verticalcontainer:{
        flexDirection:'column',
        marginLeft:10,

    },
    horizontalcontainer:{
        flexDirection:'row',
        marginTop:7,
        marginLeft:10,

    },
    image:{
        height:100,
        width:100,
        borderRadius:10,
        borderTopRightRadius:0,
        borderBottomRightRadius:0,


    },
    image2:{
        height:30,
        width:33,
        marginLeft:10,

    },
    smalltext:{
        fontSize:18,
        // textAlignVertical:'center',
        // alignSelf:'center',
        // marginTop:-20,
        width: 80,
        color:"#FFFFFF",
        fontFamily:"times"

    },
    button:{
        alignSelf:"flex-end",
        // marginRight:-40,
        // marginLeft:55,
        // marginTop: 5,
        backgroundColor: "#FF0000",
        width:100,
        height:40,
        borderRadius:5,
    },
    btntext:{
        alignSelf:'center',
        fontSize:20,
        color:"#FFFFFF",
        fontFamily:"times",
        marginTop:5,
    }
  });

export default PlayerView;

