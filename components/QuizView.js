import React from 'react';
import { TextInput,View,Text } from 'react-native';
import {StyleSheet, Image} from 'react-native';
import FormImage from './FormImage';
import FormButton from '../components/FormButton';
import { TouchableOpacity } from 'react-native-gesture-handler';

const QuizView = ({
    quizname = "Quiz Long Title is Here",
    starrating = "4.4",
    thumbnailuri = 'https://firebasestorage.googleapis.com/v0/b/visquiz-6bdd5.appspot.com/o/images%2Fdatabase%2F2107957.png?alt=media&token=2b492933-82ca-4b14-a98c-9689c6026ae8',
    onPress = null,
    type = "",
    ...more
}) => {
    return (
        <View style={styles.maincontainer}>
            <Image source={{ uri : thumbnailuri}}
            style={styles.image}
            >

            </Image>
            <View style={styles.verticalcontainer}>
                <Text style={styles.label}>{quizname}</Text>
                <View style={styles.horizontalcontainer}>
                    <Text style={styles.startext}>{starrating}</Text>
                    <Image source={{uri:"https://firebasestorage.googleapis.com/v0/b/visquiz-6bdd5.appspot.com/o/images%2Fdatabase%2F2107957.png?alt=media&token=2b492933-82ca-4b14-a98c-9689c6026ae8"}}
                    style={styles.image2}>

                    </Image>
                    <TouchableOpacity 
                    style={styles.button}
                    onPress={onPress}
                    >
                    <Text style={styles.btntext}>{type}</Text>
                    </TouchableOpacity>
                </View>
                
                

                
            </View>
            
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        // alignSelf: 'center',
        borderWidth: 1,
        // padding: 10,
        // width: 320,
        marginTop: 0,
        backgroundColor: "#FFFFFF",
    },
    label: {
        marginTop:5,
            height: 40,
            // marginLeft: 40,
            // marginTop:30,
            fontSize: 20,
            color: "#FFFFFF",
            fontFamily:"times"
    },
    maincontainer:{
        width:350,
        backgroundColor:'#7A8FFF',
        height:100,
        alignSelf:'center',
        borderRadius:10,
        flexDirection:'row',
        marginTop:15,

    },
    verticalcontainer:{
        flexDirection:'column',
        marginLeft:10,

    },
    horizontalcontainer:{
        flexDirection:'row',
        marginTop:7,
        marginLeft:10,

    },
    image:{
        height:100,
        width:100,
        borderRadius:10,
        borderTopRightRadius:0,
        borderBottomRightRadius:0,


    },
    image2:{
        height:30,
        width:33,
        marginLeft:10,

    },
    startext:{
        fontSize:30,
        textAlignVertical:'center',
        // alignSelf:'center',
        marginTop:-7,
        color:"#FFFFFF",

    },
    button:{
        alignSelf:"flex-end",
        // marginRight:-40,
        marginLeft:55,
        // marginTop: 5,
        backgroundColor: "#1E38C5",
        width:100,
        height:40,
        borderRadius:5,
    },
    btntext:{
        alignSelf:'center',
        fontSize:28,
        color:"#FFFFFF",
        fontFamily:"times"
    }
  });

export default QuizView;

