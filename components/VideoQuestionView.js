import React,{useState} from 'react';
import { TextInput,View,Text} from 'react-native';
import {StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';
import FormImage from './FormImage';
import FormButton from '../components/FormButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RadioButtons } from 'react-native-radio-buttons'
import Video from 'react-native-video';

const VideoQuestionView = ({
    question_uri = '',
    option1 = '',
    option2 = '',
    option3 = '',
    option4 = '',
    thumbnailuri = '',
    value = '',
    handleOnPress = null,
    ...more
}) => {

    const emptyFunction = (value) =>{
        console.log("Pressed " + value.substring(3))
    }

    let options= [
        "A. "+option1,
        "B. "+option2,
        "C. "+option3,
        "D. "+option4,
    ];

    function renderOption(option, selected, onSelect, index){
        const style = selected ? styles.selectedAnswer : styles.nonSelectedAnswer;
     
        return (
          <TouchableWithoutFeedback onPress={onSelect} key={index}>
            <Text style={style}>{option}</Text>
          </TouchableWithoutFeedback>
        );
      }

      function renderContainer(optionNodes){
        return <View>{optionNodes}</View>;
      }
    return (
        <View style={styles.container}>
            <Image source={{uri: thumbnailuri}}
            style={styles.image}>

            </Image>
            {/* <Text
            style = {styles.question}
            >{question}</Text> */}
            <Video source={{uri: question_uri}}   // Can be a URL or a local file.
                ref={(ref) => {
                    this.player = ref
                }}                                      // Store reference
                onBuffer={this.onBuffer}                // Callback when remote video is buffering
                onError={this.videoError}               // Callback when video cannot be loaded
                />
            {/* <Text>Selected option: {'' || 'none'}</Text> */}
            
            
        </View>
    );
};

const styles = StyleSheet.create({
    container:{
        alignContent:'center',
    },
    image:{
        height:150,
        width:250,
        borderRadius:10,
        borderTopRightRadius:0,
        borderBottomRightRadius:0,
        alignSelf:'center',

    },
    question:{
        fontSize:22,
        color: "#000000",
        width:300,
        height:60,
        textAlignVertical:'center',
        marginLeft:20,
    },
    selectedAnswer:{
        fontSize:18,
        color: "#000000",
        width:300,
        height:50,
        textAlignVertical:'center',
        marginLeft:40,
        fontWeight: 'bold',
        backgroundColor: "#8BE78B",
        borderRadius:10,
        marginTop:10,
        paddingLeft:10,
        fontFamily:"sans-serif-light",
    

    },
    nonSelectedAnswer:{
        fontSize:18,
        color: "#000000",
        width:300,
        height:50,
        textAlignVertical:'center',
        marginLeft:40,
        backgroundColor: "#FFFFFF",
        borderRadius:10,
        marginTop:10,
        paddingLeft:10,
        fontFamily:"sans-serif-light",
    }


  });

export default VideoQuestionView;

