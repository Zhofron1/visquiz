

import React from 'react';
import { TextInput,View,Text, Touchable, TouchableOpacity } from 'react-native';
import {StyleSheet} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';


const FormDropdown  = ({
    label = "",
    onChangeValue = null,
    onSelectItem = null,
    onPress = null,
    open = false,
    value = null,
    items = null,
    setOpen = null,
    setValue = null,
    setItems = null,
    ...more
}) => {
    return (
        <View style={styles.dropdown}>
            <Text style={styles.label}>{label}</Text>
            <DropDownPicker 
                style={{borderRadius:0, marginTop:10,}}
                placeholder={'Select ' + label}
                open = {open}
                value = {value}
                items = {items}
                setOpen = {setOpen}
                setValue = {setValue}
                setItems = {setItems}
                disabled={false}
                onChangeValue={onChangeValue}
                onSelectItem={onSelectItem}
                onPress={onPress}
                // maxHeight={150}
            >

            </DropDownPicker>
        </View>
    );
};

const styles = StyleSheet.create({
    label: {
        textAlign:'left',
        fontSize:18,
        marginTop:30,
        color: "#000000",
      },
      dropdown: {
        // textAlign:'center',
        // fontSize:18,
        // color: "#FFFFFF",
        width:"80%",
        // maxHeight: 40,
        alignSelf:'center',
        borderRadius:0,
        zIndex:1

      },
  });

export default FormDropdown;


