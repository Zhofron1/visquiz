import React from 'react';
import { TextInput,View,Text, Touchable, TouchableOpacity } from 'react-native';
import {StyleSheet} from 'react-native';


const FormButton  = ({
    label = "",
    handleonpress = null,
    style,
    isPrimary = true,
    ...more
}) => {
    return (
        <TouchableOpacity
        style = {{
            paddingVertical:10,
        backgroundColor: "#8CDCDA",
        borderWidth: 1,
        borderColor: "#8CDCDA",
        borderRadius:5,
        ...style
        }}
        activeOpacity={0.9}
        onPress={handleonpress}
        {...more}>
            <Text style={styles.label} >
                {label}
            </Text>

        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    label: {
        textAlign:'center',
        fontSize:18,
        color: "#FFFFFF",
      },
  });

export default FormButton;

