import React from 'react';
import { TextInput,Image,View,Text, Touchable, TouchableOpacity } from 'react-native';
import {StyleSheet} from 'react-native';


export const FormImageSelect  = ({
    handleonpress = null,
    ...more
}) => {
    return (
        <View>
            <Text style={styles.label}> Thumbnail</Text>
        <TouchableOpacity 
          style = {styles.imagebtn} 
          onPress={handleonpress}>
            <Text>+ Add Image</Text>
          </TouchableOpacity>
          </View>
    );
};

export const FormImage  = ({
    imageUri = "",
    ...more
}) => {
    return (
        <View>

        <Text style={styles.label}> Thumbnail</Text>
        <Image source={{
            uri: imageUri
          }}
          resizeMode={"center"}
          style={styles.image}>

          </Image>
          </View>
    );
};

const styles = StyleSheet.create({
    label: {
        textAlign:'center',
        fontSize:18,
        color: "#000000",
        marginLeft:-10,
      },
    image:{
        width:"37.5%",
            height:150,
            borderRadius:10,
            alignSelf:'center'
    },
    imagebtn:{
        alignSelf:'center',
            alignItems:'center',
            justifyContent:'center',
            padding:20,
            backgroundColor: "#8CDCDA",
    }
  });


export default FormImage;

