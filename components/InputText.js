import React from 'react';
import { TextInput,View,Text } from 'react-native';
import {StyleSheet} from 'react-native';


const InputText = ({
    label = "",
    placeholder = "",
    onChangeText = null,
    value = null,
    ...more
}) => {
    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            <TextInput 
            style={styles.input}
            placeholder = {placeholder}
            value = {value}
            onChangeText = {onChangeText}
            {...more}
            >

            </TextInput>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        alignSelf: 'center',
        borderWidth: 1,
        padding: 10,
        width: 320,
        marginTop: 0,
        backgroundColor: "#FFFFFF",
    },
    label: {
        height: 40,
            marginLeft: 40,
            marginTop:30,
            fontSize: 20,
            color: "#000000",
      },
  });

export default InputText;

