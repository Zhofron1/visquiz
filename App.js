import { NavigationContainer } from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import 'react-native-gesture-handler';
import LoginStackNavigator from './navigators/LoginStackNavigator';
import auth from '@react-native-firebase/auth';
import { HomeScreen } from './Menus';
import HomeStackNavigator from './navigators/HomeStackNavigator';

const App = (navigation) => {
  const[currentUser, setCurrentUser] = useState(null)
  const[isLoading, setIsLoading] = useState(true)

  const onAuthStateChanged = async user => {
    await setCurrentUser(user)
    setIsLoading(false)
  };

  useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
      return subscriber;
  })

  if(isLoading){
    return null;
  }

  return (
    <NavigationContainer>
      {
        currentUser ? <HomeStackNavigator></HomeStackNavigator> : <LoginStackNavigator></LoginStackNavigator>
      }
    </NavigationContainer>
  );
};



export default App;